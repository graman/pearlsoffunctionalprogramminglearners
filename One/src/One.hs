-----------------------------------------------------------------------------
--
-- Module      :  One
-- Copyright   :
-- License     :  AllRightsReserved
--
-- Maintainer  :
-- Stability   :
-- Portability :
--
--Consider the problem of computing the smallest natural number not in a
--given finite set X of natural numbers. The problem is a simplification of a
--common programming task in which the numbers name objects and X is
--the set of objects currently in use. The task is to find some object not in
--use, say the one with the smallest name.
-- |
--
-----------------------------------------------------------------------------

module One (
minfree,checklist,search,minfree'
) where
import qualified Data.Array as DA
import qualified Data.Array.ST as DAS
import qualified Data.List as DL
--a direct solution
--smallest number not in xs is smallest number in [0..] but not in x
minfree :: [Integer] -> Integer
minfree [] = error "MINFREE : Cannot take empty lists"
minfree xs = head (filter (\x -> not(elem x xs))[0..] )
--the above solution is not very performant as there could be O(n^2) worst-case computations

--the below is an array implementation as above is a direct o(n^2) one

{--
The key fact for both the array-based and divide and conquer solutions
is that not every number in the range [0 .. length xs] can be in xs. Thus the
smallest number not in xs is the smallest number not in filter (<= n)xs, where
n = length xs. The array-based program uses this fact to build a checklist
of those numbers present in filter (<= n) xs. The checklist is a Boolean array
with n +1 slots, numbered from 0 to n, whose initial entries are everywhere
False. For each element x in xs and provided x <= n we set the array element
at position x to True
--}
checklist :: [Integer]-> DA.Array Integer Bool
checklist xs = DA.accumArray (||) False (0, n) (zip (filter (<n) xs) (repeat True))
               where n=(toInteger.length) xs
{--
search is just finding the first false entry in the array
--}

search :: DA.Array Integer Bool -> Integer
search = toInteger.length .(takeWhile id) . (DA.elems)

minfree' :: [Integer] ->Integer
minfree' = search . checklist
--another idea is with count lists where-in we first sort the list
--to sort we make use of same array accumulator in a different fashion with values
--being 1 and indexes marked by each element of input list

countlist :: [Integer] -> DA.Array Integer Integer
countlist xs = DA.accumArray (+) 0 (0,n) (zip xs (repeat 1))
               where n = (toInteger . length) xs
sort :: DA.Array Integer Integer -> [Integer]
sort clist=  ([k|(k,x)<- DA.assocs clist,x==1])
--another version of sort as in book with replicate
sort' :: DA.Array Integer Integer -> [Integer]
sort' clist = concat [replicate (fromIntegral x) k |(k,x)<-DA.assocs clist ]

minfree'' :: [Integer] -> Integer
minfree'' xs = (fst.head .filter (\(x,y)->if y==0 then True else False). DA.assocs . countlist) xs

--another implementation of checklist using runSTArray
checklist' :: [Integer]-> DA.Array Integer Bool
checklist' xs = DAS.runSTArray(DAS.newArray (0::Integer,n) False
                >>= \x -> sequence [DAS.writeArray x a True|a<-xs, a<=n]
                >>= \_ -> return x)
                where n=toInteger.length $ xs
{--
The below is a divide and conquer implementation
of minfree.The idea uses set-theoritic isomorphism for lists.
if as and us are dis-join sets(or lists) and so are bs and vs.
then (as++bs) // (us++vs) == (as//us) ++ (bs//vs)
using the above isomorphism we partition the set into 2 sub-sets by (b)

(us,vs) = Data.List.partition (<b) ([0..b-1]++[b..])

now to check minfree , all we need to check is using the below:
[0..] // xs = ([0..b-1]//us) ++ ([b..]//vs)
which implies
head ([0..]//xs) = head ([0..b-1]//us) ++ ([b..]//vs)
which is
head ([0..]//xs) = if null ([0..b-1]//us) then head(([b..]//vs)) else head ([0..b-1]//us)
but here is where we further try to optimize based on an assumption that xs does not contain
duplicates.
If its so, then the only way ([0..b-1]//us) is null would be with length us=b.

Further to help reduce a good recurisive solution to this issue, we create another function

minfreeFrom a xs  and instance for minfree xs = minfreeFrom 0 xs

Also best possible partion for b is as b= 1+ n/2 where n= length xs
--}
minfreeFrom :: Integer -> [Integer] ->Integer
minfreeFrom a xs | null xs = a
                 | toInteger(length us) ==b = minfreeFrom b vs
                 | otherwise = minfreeFrom a us
                 where (us,vs)=DL.partition (<b) xs
                       b=a +1+toInteger(n `div` 2)
                       n=toInteger.length $ xs
