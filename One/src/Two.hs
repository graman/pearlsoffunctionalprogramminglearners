-----------------------------------------------------------------------------
--
-- Module      :  Two
-- Copyright   :
-- License     :  AllRightsReserved
--
-- Maintainer  :
-- Stability   :
-- Portability :
--
--By definition, a surpasser of an element of an array
--is a greater element to the right, so x [j ] is a surpasser of x [i] if i < j
--and x [i ] < x [j ]. The surpasser count of an element is the number of its
--surpassers.Rem’s problem is to compute the
--maximum surpasser count of an array of length n > 1 and to do so with an
--O(n log n) algorithm
-- |
--
-----------------------------------------------------------------------------

module Two (
tails
) where
import qualified Data.List.Utils as LU
--this is unlinke Data.List.tails as pattern for [] = [] while Data.List.tails has [[]]
tails :: [a] -> [[a]]
tails [] =[]
tails (x:xs) = (x:xs):tails xs
{--
The simplest solution is to just plot the surpasser count as below
Its a quadratic solution
--}
msc :: Ord a => [a] -> Integer
msc xs = maximum [scount z zs | z:zs <- tails xs ]
scount z zs = toInteger.length.(filter(z<))$zs
{--
Equational reasoning on a divide and conquer approach:

divide implies list concat

We cannot divide msc across list-concats in join op like below.

msc (xs++ys) <> join (msc xs)(msx ys)

However if we have table as  map of scounts on each z as--}

--table xs = [(z,scount z zs) | (z:zs) <- tails (xs)]
--in which msc becomes
--msc' = maximum .(map snd).table$
{--
this however is still quadratic in complexity(time) .
We can however see an equational logic arising for table(xs++ys)
expressed as join(table xs) (table ys).

But there is one observation wrt tails that needs to be ensured.
The fact that
tails (xs++ys) = (map (++ys) (tails xs)) ++ tails ys
and thus equational reductions on table for join based reasoning leads to:

table(xs++ys) = [(z,scount z zs)| (z:zs)<- tails(xs++ys)]
              = [(z,scount z zs)| (z:zs)<- (map (++ys) (tails xs)) ++ tails ys]
              = [(z,scount z (zs++ys))| (z:zs)<- tails xs] ++ [(z,scount z zs)| (z:zs)<-tails ys]
and further focusing on
[(z,scount z zs)| (z:zs)<- (map (++ys) (tails xs))]
as join . (map map(ys++) xs) == map(++ys) . join xs , hence
=[(z,scount z (zs++ys))| (z:zs)<- tails xs]
as scount z (zs++ys) == scount z zs + scount z ys
=[(z,(scount z zs) + (scount z ys)))| (z:zs)<- tails xs]
but ys = map fst table ys
and if c = snd.table(xs)
=[(z,c+scount z (map fst table ys)|(z,c)<-table xs]
and
[(z,scount z zs)| (z:zs)<-tails ys]=  table ys

hence over all
table(xs++ys)=[(z,c+scount z (map fst table ys)|(z,c)<-table xs] ++ table ys
thus we redifine join as
--}
--join txs tys = [(z , c + tcount z tys) | (z , c)?txs] ++ tys
-- where tcount z tys = scount z (map fst tys)
{--
this however is not optimized as join still takes quadratic time over txs and tys
thus seeking to optimize tcount
tcount z tys = scount z (map fst tys)
=length (filter (<z) (map fst tys))
since filter p . map f = map f. filter (p.f)
=length (map fst . filter ( (<z). fst) tys)
since length. map f = length
=length(filter ((<z). fst) tys)

hence redefining tcount
--}
--tcount z tys = length (filter ((<z). fst) tys)
{-- further-more if tys were sorted then filter can be reduced to a dropWhile
tcount' z tys = length (dropWhile ((<z).fst) tys)
--}
{-- thus redifining table for sorted order we have --}
--table' xs = sort[(z, scount z zs)|z:zs <- xs]
{--and for the sorted table we need to merge instead of concat and thus --}
--join' txs tys = [(z , c + tcount z tys) | (z , c)<-txs] `merge` table ys
--reducing the above join will give rise to

table [x] = [(x , 0)]
table xs = join (m - n) (table ys) (table zs)
           where m = length xs
                 n = m `div` 2
                 (ys, zs) = splitAt n xs
join 0 txs [] = txs
join n [] tys = tys
join n txs@((x , c) : txs') tys@((y, d) : tys')
                                 | x < y = (x , c + n) : join n txs' tys
                                 | otherwise = (y, d) : join (n-1) txs tys'

{--
The final reduction is based on the fact that a merge would place the least first
and that
join' txs tys = [(z , c + tcount z tys) | (z , c)<-txs] `merge` table ys
would be equal to
= (z,c+tcount z tys):[(z , c + tcount z tys) | (z , c)<-txs] `merge` (y,scount y ys):[(z, scount z zs)|z:zs <- ys]
and the first element would depend on which of these is smaller
--}
