# README #

This is a record of all pearls of functional programming in Haskell as am learning them one by one.

### What is this repository for? ###

* Personal record of all pearls
* Version 0.01


### This contains the following pearls ###

#### Computing the smallest natural number not in a given finite set

* Array based solution

* Divide and Conquer based solution


### Contribution guidelines ###

*None

### Who do I talk to? ###

* Ganesh Raman